#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <util/delay.h>
#include <i2c_master.h>

//definition der srf utraschall mess geraet dings adresse
#define SRF02_DEVICE_ADDRESS (0xE0)

//------------------------------------LED init+display-----------------------------------

void led_init(void){
    //gesamter PortB sowie PinA1 als Ausgang
    DDRB = (1<<PB7) | (1<<PB6) | (1<<PB5) | (1<<PB4) | (1<<PB3) | (1<<PB2) | (1<<PB1) | (1<<PB0);
    DDRA = (1<<PA1);
}

void led_display(uint8_t number){
    //lasse led anzahl von "number" im bereich [0,8] gleichzeitig aufleuchten
    //zunaechst alle LEDs einmal abstellen
    PORTB = (0<<PB7) | (0<<PB6) | (0<<PB5) | (0<<PB4) | (0<<PB3) | (0<<PB2) | (0<<PB1) | (0<<PB0);
    PORTA = (0<<PA1);
    //nun, je nach Anzahl entscheidet der switch wie viele LEDs leuchten sollen
    switch(number){
        //gelbe LEDs
        case 1: PORTB = (1<<PB0);
        case 2: PORTB = (1<<PB1) | (1<<PB0);
        case 3: PORTB = (1<<PB2) | (1<<PB1) | (1<<PB0);
        case 4: PORTB = (1<<PB3) | (1<<PB2) | (1<<PB1) | (1<<PB0);
        //gruene LEDs
        case 5: PORTB = (1<<PB4) | (1<<PB3) | (1<<PB2) | (1<<PB1) | (1<<PB0);
        case 6: PORTB = (1<<PB5) | (1<<PB4) | (1<<PB3) | (1<<PB2) | (1<<PB1) | (1<<PB0);
        case 7: PORTB = (1<<PB6) | (1<<PB5) | (1<<PB4) | (1<<PB3) | (1<<PB2) | (1<<PB1) | (1<<PB0);
        case 8: PORTB = (1<<PB7) | (1<<PB6) | (1<<PB5) | (1<<PB4) | (1<<PB3) | (1<<PB2) | (1<<PB1) | (1<<PB0);
        //rote LED
        default: PORTA = (1<<PA1);   
    }
    //vllt. klappt das so besser ? irgendwie in der Art
    /*if(number!=0){
        for(int i = 0;i<number;i++){
            PORTB |=(1<<PB(char)i);
        }
    }
    else{
        PORTA = (1<<PA1);
    }*/

    //fuer beide loesungen gilt: nicht schoen, aber selten
}


//------------------------------------ADC init+read--------------------------------------

void adc_init(void){
    //adc aktivieren und prescaler mit Teilungsfaktor 128 einrichten
    ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); //prescaler = 128
    //versorgungsspannung VCC=5V als Referenzspannung setzen
    ADMUX = (1<<REFS0);
    //blindumwandlung: starten des wandlungsprozesses mit folgendem auslesen des ADC (um falsche daten zu überschreiben)
}

uint16_t adc_read(uint8_t channel){
 //übergebenen kanal als kanal für adc wandlung einstellen
    channel &= 0b00000111;  // AND operation mit 7 haelt den kanal immer zwischen 0 und 7
    ADMUX = (ADMUX & 0xF8)|channel; // clears the bottom 3 bits before OR-ing
 
    //wandlung starten + ergebnis abfragen
    ADCSRA |= (1<<ADSC);
    // wait for conversion to complete
    // ADSC becomes ’0′ again
    // till then, run loop continuously
    while(ADCSRA & (1<<ADSC));
    //übergtragene Spannung als return
    return (ADC);
}

uint8_t getMinDistance(){
    
}
//------------------------------------I2CBus + SRF02 Sensor--------------------------------------

void i2c_writeRegister(uint8_t add,uint8_t reg,uint8_t dat){
    //I2C kommunikation starten mit gewünschter adresse add
    i2c_start(add);
    //gewünschte register nummer reg senden
    i2c_write(reg);
    //datenbyte dat senden
    i2c_write(dat);
    //kommunikation beenden
    i2c_stop();
}

void i2c_readRegister(uint8_t add, uint8_t reg){
    //festlegen des gewünschten Registers (wie in writeRegister)
    i2c_start(add);
    i2c_write(reg);
    i2c_stop();
    //i2c kommunikation an addresse add starten
    i2c_rep_start(add);
    //byte vom Bus lesen und in temp. variable uint8_t dat schreiben (lesen ohne ACK)
    uint8_t dat = i2c_readNak();
    //i2c kommunikation beenden
    i2c_stop();
    //daten aus dat return
    return dat;
}

uint16_t srf02_read(void){
    //i2c methoden im Datenblatt des sensors zur abstands-messung in cm
    i2c_start(SRF02_DEVICE_ADDRESS | 1);
    uint8_t HRange= 2;
    uint8_t LRange= 3;
    uint8_t HValue = i2c_readNak(&HRange);
    uint8_t LValue = i2c_readNak(&LRange);
    //concatenate HRange(8bit) with LRange(8bit) to consist in 16bit
    uint16_t value = ((uint16_t)HValue << 8)|LValue;
    i2c_stop();
    return value;
}

//------------------------------------Gesamtverhalten--------------------------------------
int main(void){
    
    //atmega initialisieren
    atmega_init();
    //i2c bus initialisieren
    i2c_init();
    //endless loop
    while(1){
        //take one analog reading from channel 0
        ADvalue = adc_read(0);
        //take one distance reading
        curDist = srf02_read();
        //set the max distance to 63cm
        maxDist= 63;
        //sets the min dist depending on the analog value (ADvalue) to a value
        //smaller than (maxDist-8)
        minDist= fmin(ADvalue/8,maxDist-8);
        //sets the adjusted range
        range=

        //activate computed number of LEDs corresponding to the measured
        //distance and actual range

        //display red LED if measured distance is smaller than minimal
        //distance
        if(curDist < minDist){
            led_display(99);
        }

        _delay_ms(50)
 
    }
}
