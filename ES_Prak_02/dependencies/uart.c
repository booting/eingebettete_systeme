#include "uart.h"


#define USART_RX_BUFFER_SIZE 256     /* 2,4,8,16,32,64,128 or 256 bytes */
#define USART_TX_BUFFER_SIZE 256     /* 2,4,8,16,32,64,128 or 256 bytes */
#define USART_RX_BUFFER_MASK ( USART_RX_BUFFER_SIZE - 1 )
#define USART_TX_BUFFER_MASK ( USART_TX_BUFFER_SIZE - 1 )
#if ( USART_RX_BUFFER_SIZE & USART_RX_BUFFER_MASK )
	#error RX buffer size is not a power of 2
#endif
#if ( USART_TX_BUFFER_SIZE & USART_TX_BUFFER_MASK )
	#error TX buffer size is not a power of 2
#endif

static unsigned char USART_RxBuf[USART_RX_BUFFER_SIZE];
static volatile unsigned char USART_RxHead;
static volatile unsigned char USART_RxTail;
static unsigned char USART_TxBuf[USART_TX_BUFFER_SIZE];
static volatile unsigned char USART_TxHead;
static volatile unsigned char USART_TxTail;

void uart_init( void ) {

	UCSRB |= ( 1<<RXEN );			// Aktivierung des UART_Empf�nger_Pins
	UCSRB |= ( 1<<TXEN );			// Aktivierung des UART_Sender_Pins
	UCSRB |= ( 1<<RXCIE );		// Aktivierung des "RX Complete Interrupt" bei Empfang von Daten
	UCSRC |= (3<<UCSZ0);			// Einstellung einer Blockgroesse von 8 Bit

	UBRRH = UBRR_BAUD >> 8;		// Setzen der Taktfrequenz
    UBRRL = UBRR_BAUD & 0xFF;


	USART_RxTail = 0;				// Setzen der aktuellen Pakete auf 0
	USART_RxHead = 0;
	USART_TxTail = 0;
	USART_TxHead = 0;
}


void uart_sendchar( unsigned char c ) {

	unsigned char tmphead;
	tmphead = ( USART_TxHead + 1 ) & USART_TX_BUFFER_MASK;
	while ( tmphead == USART_TxTail );

	USART_TxBuf[tmphead] = c;
	USART_TxHead = tmphead;

	UCSRB |= (1<<UDRIE); 			// Aktivierung "UART Datenregister Leer Interrupt"
									// d.h. UART ist wieder bereit ein neues Zeichen zu senden
}
 
void uart_sendstring ( char *s ) {
	while ( *s ) {
		uart_sendchar( *s );
		s++;
    }
	uart_sendchar( 0 );
}

uint8_t uart_receive( void ) {

	unsigned char tmptail;
	
	while ( USART_RxHead == USART_RxTail );


	tmptail = ( USART_RxTail + 1 ) & USART_RX_BUFFER_MASK;

	USART_RxTail = tmptail;

	return USART_RxBuf[tmptail];
	
	return 0;
}

uint8_t DataInReceiveBuffer( void ) {
	return ( USART_RxHead != USART_RxTail );
}


ISR(USART_RXC_vect) {

	unsigned char data;
	unsigned char tmphead;

	data = UDR;                 

	tmphead = ( USART_RxHead + 1 ) & USART_RX_BUFFER_MASK;
	USART_RxHead = tmphead;      /* Store new index */

	if ( tmphead == USART_RxTail )
	{
		/* ERROR! Receive buffer overflow */
	}

	USART_RxBuf[tmphead] = data;
}

ISR(USART_UDRE_vect) {
	unsigned char tmptail;

	if ( USART_TxHead != USART_TxTail )
	{
		tmptail = ( USART_TxTail + 1 ) & USART_TX_BUFFER_MASK;
		USART_TxTail = tmptail;

		UDR = USART_TxBuf[tmptail];
	}
	else
	{
		UCSRB &= ~(1<<UDRIE);
	}
}

