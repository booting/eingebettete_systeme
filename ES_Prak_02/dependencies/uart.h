/** 
 *	@file uart.h  
 */

#ifndef UART_H
#define UART_H

#include <avr/io.h>
//#include <stdint.h>
#include <avr/interrupt.h> 

#ifndef F_CPU
#define F_CPU 1000000UL
#endif

#ifndef BAUD
#define BAUD 4800UL
#endif

// Berechnungen
/** Formel f�r die Berechnung Geschwindigkeit f�r das UBRR-Register
 */
#define UBRR_BAUD ((F_CPU+BAUD*8)/(BAUD*16)-1)		// clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_BAUD+1)))			// Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD)			// Fehler in Promille, 1000 = kein Fehler.

#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
#error Systematischer Fehler der Baudrate groesser 1% und damit zu hoch!
#endif

/** \brief UART Initialisierung
 *
 * Initialisiert UART0, schaltet RX und TX ein, setzt �bertragungsformt (8N1)
 */
void uart_init( void );


/** \brief Sendet ein Zeichen
 *
 * Sendet ein �bergebenes Zeichen �ber die serielle Schnittstelle
 *
 * @param[in] c Das zu sendende Zeichen
 */
void uart_sendchar( unsigned char c );

/** \brief Sendet eine Zeichenkette
 *
 * Sendet die �bergebene Zeichenkette zeichenweise mit der uart_sendchar Methode
 *
 * @param[in] s Die zu sendende Zeichenkette
 */ 
void uart_sendstring ( char *s );

/** \brief Empf�ngt ein Zeichen
 *
 * Empf�ngt ein Zeichen von der seriellen Schnittstelle
 *
 * @param[out] return Das empfangene Zeichen
 */
uint8_t uart_receive( void );

/** \brief Gibt an, ob ein Zeichen im Buffer ist
 *
 * @param[out] return Anzahl der Zeichen im Buffer
 */
uint8_t DataInReceiveBuffer( void );
#endif
