/*
* SpaceInvaders.c
*
* Created: 22.05.2014 16:26:08
*  Author: Ehlers
*/


#define F_CPU 1000000UL

/* ---------------------------------------------- */
// Includes
#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>
#include "lcd.h"
#include "avrx.h"
#include "hardware.h"
#include "avrx-io.h"
#include "avrx-signal.h"

/* ---------------------------------------------- */
// Definitionen
#define NUMBER_ALIEN_SPECIES 4
#define SYMBOL_SPACESHIP 1
#define SYMBOL_SPACESHIP_FIRE 2
#define SYMBOL_EXPLOSION 3
#define SYMBOL_MISS 4

/* ---------------------------------------------- */
// Globale Variablen
// Position des Aliens in der erste Reihe
uint8_t alienPosition;
// Spezies des Aliens
uint8_t alienSpecies = 5;
// Position des Raumschiffes in der zweiten Reihe
uint8_t spaceshipPosition;
// Fehlschuesse
uint8_t spaceshipRemainingEnergie;
// Punkte des Raumschiffes
uint8_t spaceshipPoints;
// Spielzeit
uint8_t playTime;
// Variablen zur Spielsteuerung
uint8_t gameEnd;
uint8_t fire;
uint8_t alienBlocked = 0;



/* ---------------------------------------------- */
// semaphoreData: Mutual exclusion der globalen Variablen
AVRX_MUTEX(semaphoreData);
// semaphoreStart: Start des Spiels
AVRX_MUTEX(semaphoreStart);

/* ---------------------------------------------- */
// Deklaration der Timer (Ein Timer kann nicht in mehreren Prozessen/Tasks genutzt werden)
TimerControlBlock gameTimer, alienTimer, positionTimer, tasterTimer, TickTimerLCD;


/* ---------------------------------------------- */
// Deklaration der Nachrichten und der MessageQueue
MessageControlBlock UpdateDisplay;
MessageQueue LcdQueue;

/* ---------------------------------------------- */
// AD-Wandlung
uint16_t adc_read()
{
	// AD-Wandlung starten
	ADCSRA |= (1<<ADSC);
	// auf Abschluss der Konvertierung warten
	while (ADCSRA & (1<<ADSC));
	// Ergebnis auslesen, umrechnen in Bereich 0-14 und zur�ckgeben
	return ADCW;
}

/* ---------------------------------------------- */
// Initialisierung des AD Wandlers
void adc_init()
{
	// PINA0 als Eingang festlegen
	DDRA &= ~1;
	// Vcc als Referenzspannung
	ADMUX = (1<<REFS0);
	// Aktivieren und Pre-Scaler auf 128
	ADCSRA = (1<<ADEN) | (1<<ADPS0) | (1<<ADPS1) | (1<<ADPS2);
	// Dummy-Wandlung
	adc_read(0);
}

/* ---------------------------------------------- */
// Interrupt Handler f�r die Timer
AVRX_SIGINT(TIMER0_OVF_vect)
{
	IntProlog();
	TCNT0 = TCNT0_INIT;
	AvrXTimerHandler();
	Epilog();
}

/* ---------------------------------------------- */
// Initialisierungen
void init_function() {
	MCUCR = _BV(SE);
	TCNT0 = TCNT0_INIT;
	TCCR0 = TMC8_CK256;
	TIMSK = _BV(TOIE0);
	
	DDRA  = DDRA  & ~(1 << PINA1);
	PORTA = PORTA |  (1 << PINA1);
}


/* ---------------------------------------------- */
// Zeitmessung
AVRX_GCC_TASKDEF(taskClock, 30, 0)
{
	while(1) {
		AvrXWaitSemaphore(&semaphoreStart);
		AvrXWaitSemaphore(&semaphoreData);
		playTime = 60;
		AvrXSetSemaphore(&semaphoreData);
		AvrXSetSemaphore(&semaphoreStart);
		
		while(1) {
			AvrXDelay(&gameTimer, 1000); // wait 1 sec
			AvrXWaitSemaphore(&semaphoreData);
			
			if(gameEnd) {
				AvrXSetSemaphore(&semaphoreData);
				break;
			} else if(playTime == 0) {
				gameEnd = 1;
				AvrXWaitSemaphore(&semaphoreStart);
				AvrXSetSemaphore(&semaphoreData);
				AvrXSendMessage(&LcdQueue, &UpdateDisplay); // update display
				break;
			}
			
			AvrXSetSemaphore(&semaphoreData);
			AvrXSendMessage(&LcdQueue, &UpdateDisplay); // update display
		}
	}
}

/* ---------------------------------------------- */
// Steuerung des Aliens
AVRX_GCC_TASKDEF(taskAlien, 60, 0)
{
		while(1) {
			AvrXDelay(&alienTimer, 1000);
			AvrXWaitSemaphore(&semaphoreData);
			
			if(gameEnd) {
				AvrXSetSemaphore(&semaphoreData);
				continue;
			}
			uint8_t oldpos = alienPosition;
			alienPosition = random() % 14; // update pos
			alienBlocked = 0;
			AvrXSetSemaphore(&semaphoreData);
			if(alienPosition != oldpos)
				AvrXSendMessage(&LcdQueue, &UpdateDisplay); // update display
		}
}

/* ---------------------------------------------- */
// Steuerung des Raumschiffes
AVRX_GCC_TASKDEF(taskSpaceship, 60, 0)
{
		while(1) {
			AvrXDelay(&positionTimer, 20);
			AvrXWaitSemaphore(&semaphoreData);
			
			if(gameEnd) {
				AvrXSetSemaphore(&semaphoreData);
				continue;
			}
			
			// position in [0,13]
			uint16_t oldpos = spaceshipPosition;
			uint16_t spaceshipPosition = adc_read()/74; // update pos
			AvrXSetSemaphore(&semaphoreData);
			
			if(spaceshipPosition != oldpos)
				AvrXSendMessage(&LcdQueue, &UpdateDisplay); // update display
		}
}


/* ---------------------------------------------- */
// Taster
AVRX_GCC_TASKDEF(taskButton, 60, 0) {
	uint8_t oldstate, newstate;
	oldstate = PINA1;
	
	while(1) {
			AvrXDelay(&tasterTimer, 100);
			newstate = PINA & PINA1;
			if(!oldstate || newstate) // no falling edge
				continue;
			
			// falling edge
			AvrXWaitSemaphore(&semaphoreData);
			
			if(gameEnd) {
				gameEnd = 0;
				playTime = 60;
				spaceshipPoints = 0;
				fire = 0;
				spaceshipRemainingEnergie = 10;
				AvrXSetSemaphore(&semaphoreData);
				AvrXSetSemaphore(&semaphoreStart); // start game
				AvrXSendMessage(&LcdQueue, &UpdateDisplay); // update display
				continue;
			}
			
			// fire!
			if(spaceshipPosition == alienPosition) { // hit
				spaceshipPoints++;
				fire = 1;
				if(spaceshipRemainingEnergie < 20)
					spaceshipRemainingEnergie++;
				alienSpecies = (rand() % NUMBER_ALIEN_SPECIES) + 5; // new alien
			} else { // miss
				fire = 2;
				spaceshipRemainingEnergie--;
			}
			
			if(spaceshipRemainingEnergie == 0) {
				gameEnd = 2;
				AvrXWaitSemaphore(&semaphoreStart);
			}
			
			AvrXSetSemaphore(&semaphoreData);
			AvrXSendMessage(&LcdQueue, &UpdateDisplay); // update display
	}
}

/* ---------------------------------------------- */
// LCD Ansteuerung
AVRX_GCC_TASKDEF(taskLcd, 100, 4)
{
	MessageControlBlock *p;
	while (1)
	{
		p = AvrXWaitMessage(&LcdQueue);	// Wait for messages in the message queue
		if (p == &UpdateDisplay) {			// Is the message the one we are waiting for?
			if(gameEnd)
			{
				AvrXWaitSemaphore(&semaphoreData);
				lcd_clear();
				char buf[16];
				switch (gameEnd)
				{
					case 1:
					lcd_setcursor(3,1);
					snprintf(buf,16,"Time Over!");
					lcd_string(buf);
					break;
					case 2:
					lcd_setcursor(2,1);
					snprintf(buf,16,"No Ammo Left!");
					lcd_string(buf);
					break;
				}
				lcd_setcursor(3,2);
				snprintf(buf,16,"Points: %u", spaceshipPoints);
				lcd_string(buf);
				
				AvrXDelay(&TickTimerLCD, 2000);
				AvrXSetSemaphore(&semaphoreData);
				
				AvrXWaitSemaphore(&semaphoreStart);
			}
			else
			{
				AvrXWaitSemaphore(&semaphoreData);
				
				lcd_clear();
				lcd_setcursor(14,1);
				char buf[3];
				if(playTime < 10)
				{
					snprintf(buf,3,"0%u", playTime);
				}
				else
				{
					snprintf(buf,3,"%u", playTime);
				}
				lcd_string(buf);
				
				lcd_setcursor(14,2);
				if(spaceshipRemainingEnergie < 10)
				{
					snprintf(buf,3,"0%u", spaceshipRemainingEnergie);
				}
				else
				{
					snprintf(buf,3,"%u", spaceshipRemainingEnergie);
				}
				lcd_string(buf);
				
				if(fire)
				{
					lcd_setcursor(spaceshipPosition,2);
					lcd_data(SYMBOL_SPACESHIP_FIRE);
					if(fire == 1)
					{
						lcd_setcursor(alienPosition,1);
						lcd_data(SYMBOL_EXPLOSION);
						alienBlocked = 1;
					}
					else
					{
						lcd_setcursor(alienPosition,1);
						lcd_data(alienSpecies);
						lcd_setcursor(spaceshipPosition,1);
						lcd_data(SYMBOL_MISS);
					}
					AvrXDelay(&TickTimerLCD, 250);
					fire = 0;
				}
				else
				{
					if(!alienBlocked)
					{
						lcd_setcursor(alienPosition,1);
						lcd_data(alienSpecies);
					}
					
					lcd_setcursor(spaceshipPosition,2);
					lcd_data(SYMBOL_SPACESHIP);
				}
				
				AvrXSetSemaphore(&semaphoreData);
			}
		}
	}
}

/* ---------------------------------------------- */
int main(void)
{
	// Initialisierungen
	
	// Definition des Stack Pointers der main
	AvrXSetKernelStack(0);
	// Initialisierung von AvrX
	init_function();
	// Initialisierung der LEDs
	lcd_init();
	// Initialisierung des ADW
	adc_init();
	
	// Definition und Initialisierung des Raumschiffes und der Aliens
	uint8_t symbols[] = {0x00, 0x00, 0x00, 0x04, 0x0A, 0x0A, 0x1B, 0x15,
		0x04, 0x04, 0x0A, 0x04, 0x0A, 0x0A, 0x1B, 0x15,
		0x0A, 0x00, 0x11, 0x0A, 0x0A, 0x11, 0x00, 0x0A,
		0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00,
		0x00, 0x00, 0x00, 0x0E, 0x15, 0x1B, 0x0E, 0x1B,
		0x00, 0x11, 0x1F, 0x15, 0x1F, 0x0A, 0x0E, 0x0A,
		0x00, 0x00, 0x0E, 0x15, 0x0E, 0x04, 0x0A, 0x11,
		// Die folgende Zeile ersetzen, um den eigenen Alien zu erstellen
	0x00, 0x00, 0x04, 0x0A, 0x11, 0x15, 0x0A, 0x15};
	
	for(int i = 0; i < 8; i++)
	{
		lcd_generatechar(i+1,symbols+(i*8));
	}
	
	// Starten des Tasks
	// TODO
	
	// Setzen der Semaphore
	// TODO
	
	// Aufrufen des Schedulers
	Epilog();
	while(1);
}