#define CPUCLK 1000000L
#define TICKRATE 1000

#define LED PORTB
#define LEDDDR DDRB

#define SWITCH PINA
#define SWITCHP PORTA

#define TCNT0_INIT (0xFF-CPUCLK/256/TICKRATE)
#define TMC8_CK256 (1<<CS02)


