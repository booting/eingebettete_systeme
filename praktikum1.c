/************************************************************************/
/* ES2.c                                                                */
/* Created: 07.04.2015 15:10:52                                         */
/* Author: ES-Student     												*/
/************************************************************************/
/* Definitions                                                          */
/************************************************************************/

#define F_CPU 1000000UL

#define I2C_DEVICE_ADDRESS			(0xE0)

#define I2C_DEVICE_REG_CMD			(0)
#define I2C_DEVICE_REG_DATAH		(2)
#define I2C_DEVICE_REG_DATAL		(3)

#define I2C_DEVICE_CMD_START_IN		(0x50)
#define I2C_DEVICE_CMD_START_CM		(0x51)
#define I2C_DEVICE_CMD_START_MS		(0x52)

/************************************************************************/
/* Include section                                                      */
/************************************************************************/

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include "i2c_master.h"
#include "uart.h"

/************************************************************************/
/* Method declarations                                                  */
/************************************************************************/

void led_init(void);
void led_display(uint8_t number);

void adc_init(void);
uint16_t adc_read(uint8_t channel);

void atmega_init(void);

void i2c_writeRegister(uint8_t add, uint8_t reg, uint8_t dat);
uint8_t i2c_readRegister(uint8_t add, uint8_t reg);

uint16_t srf02_read(void);

/************************************************************************/
/* Global variables declarations                                        */
/************************************************************************/

char buf[40];
uint16_t ADvalue, minDist, maxDist, curDist, range;

/************************************************************************/
/* Method implementations                                               */
/************************************************************************/

/**
* This method initializes the I/O-Port for the LEDs.
*
* @param	void
*/
void led_init(void)
{
    // set full Port-B as output
    DDRB = 0xFF;
    // set PA0 as input and PA1 as output
    DDRA &= ~1;
    DDRA |= 0b10;
}

/**
* This method activates the given number of LEDs.
*
* @param	number		total count of LEDs
*/
void led_display(uint8_t number)
{
    PORTA = 0xFF << (8 - number);
}

/**
* This method initializes the analog-digital-converter and performs one
* dummy reading.
* Uref=Vcc
* Prescaler=128
*
* @param	void
*/
void adc_init(void)
{
    ADMUX = (1 << REFS0); // Vref = Vcc
    ADCSRA = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // 1/128 * clk
    ADCSRA |= (1<<ADEN);

    ADCSRA |= (1<<ADSC);
    while (ADCSRA & (1<<ADSC) ) ; // "Blindumwandlung"
    (void) ADCW; // throw away result
}

/**
* This method takes one analog reading.
*
* @param	channel		analog channel wich will be read
* @return				10-bit converted value
*/
uint16_t adc_read(uint8_t channel)
{
    ADMUX = (ADMUX & (0xFF << 5)) | channel; // select channel

    ADCSRA |= (1<<ADSC);
    while (ADCSRA & (1<<ADSC) ) ;

    return ADCW;
}

/**
* This method initializes the Atmega and activates the interrupts.
*
* @param	void
*/
void atmega_init(void)
{
	sei();
	led_init();
	adc_init();
	uart_init();
	i2c_init();
}

/**
* This method writes given byte to given device and register.
*
* @param	add		Address of the device to write.
* @param	reg		Register to write.
* @param	data	Data to write.
*
*/
void i2c_writeRegister(uint8_t add, uint8_t reg, uint8_t dat)
{
    // start writing I�C communcation with device at address add
    i2c_start(add);
    // write register address
    i2c_write(reg);
    // write data
    i2c_write(dat);
    // stop I�C communication
    i2c_stop();
}

/**
* This method reads one byte from the given device and register.
*
* @param	add		Address of the device which should be read from.
* @param	reg		Register number which should be read.
*
* @return			Returns the 8-bit value which was read.
*/
uint8_t i2c_readRegister(uint8_t add, uint8_t reg)
{
    // start writing I�C communcation with device at address add
    i2c_start(add);
    // write register address
    i2c_write(reg);
    // repeat start reading I�C communcation with device at address add
    i2c_rep_start(add | 1);
    // read data from device
    uint8_t result;
    i2c_read_Nak(&result);
    // stop I�C communication
    i2c_stop();
    // return data
    return result;
}

/**
* This method takes one reading from SRF02 ultrasound sensor.
*
* @param	void
*
* @return	Returns the 16-bit distance value.
*/
uint16_t srf02_read(void)
{
	// start measuring
	i2c_writeRegister(I2C_DEVICE_ADDRESS, 0, 0x51);
	// wait until sensor finished measuring
	_delay_ms(65); // measurement needs 65 ms
	// read distance in cm from sensor
	uint16_t result;
        result = i2c_readRegister(I2C_DEVICE_ADDRESS, 2);
        result <<= 8;
        result |= i2c_readRegister(I2C_DEVICE_ADDRESS, 3);
	// return data
	return result;
}

uint16_t clip3(uint16_t floor, uint16_t x, uint16_t ceil) {
    return x > floor ? (x < ceil ? x : ceil) : floor;
}

/************************************************************************/
/* MAIN                                                                 */
/************************************************************************/
int main(void)
{
    atmega_init();
    
    int leds = 0;
    
    // endless loop
    while(1)
    {
        /////////////////////////////////////////////////////////////////////////
        //////// Kommentieren Sie jeweils ein gew�nschtes Verhalten aus: ////////
        /////////////////////////////////////////////////////////////////////////
        
        //////// Teilaufgabe 1 ////////
        // led_display(leds);
        // leds = leds < 8 ? leds++ : 0;

        //////// Teilaufgabe 2 ////////
        // led_display(adc_read(0) >> (16-3));
        
        //////// Teilaufgabe 3 ////////
        // snprintf(buf,40,"ADvvalue %u : curDist %u",adc_read(0), srf02_read());
        // uart_sendstring(buf);
        
        //////// Gesamtverhalten ////////
        // take one analog reading from channel 0
        // ADvalue = adc_read(0);
        //
        // take one distance reading
        // curDist = srf02_read();
        //
        // set the max distance to 63cm
        // maxDist = 63;
        //
        // sets the min distance depending on the analog value to a value
        // smaller than (maxDist-8)
        // minDist = ADvalue < (maxDist-8) ? ADvalue : (maxDist-8);
        //
        // sets the adjusted range
        // range = maxDist - minDist;
        //
        // activate computed number of LEDs corresponding to the measured
        // distance and actual range
        // led_display( (clip3(minDist, curDist, maxDist) - minDist) * 8 / range );
        //
        // flash red LED if measured distance is smaller than minimal distance
        // if( curDist < minDist ) {
        //     if( leds )
        //         PORTA |= 0b10;
        //     else
        //         PORTA &= ~0b10;
        // } else
        //     PORTA &= ~0b10;
        // 
        // leds = !leds;
        
        _delay_ms(50);
    }
}


/************************************************************************/
/* Interrupts                                                           */
/************************************************************************/